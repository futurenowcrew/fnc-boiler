/**
 * @Author: Fabre Ed
 * @Date:   2017-12-21T19:31:20-05:00
 * @Email:  edwidgefabre@gmail.com
 * @Filename: run_ping.js
 * @Last modified by:   Fabre Ed
 * @Last modified time: 2017-12-21T19:32:34-05:00
 */

var runPing = function(opts) {
  console.log(opts);
}

exports.runPing = runPing;
