var jsonfile = require( 'jsonfile' )
const inquirer = require( 'inquirer' );
const path = require( 'path' );
var parsedPath;

var runNPMInit = function ( opts ) {
  parsedPath = path.parse( opts.path );
  var questions = [ {
      type: 'input',
      name: 'name',
      message: "Package Name:",
      filter: function ( val ) {
        return val.toLowerCase();
      },
      default: function () {
        return parsedPath.base;
      }
    },
    {
      type: 'input',
      name: 'version',
      message: "Version:",
      default: function () {
        return '0.0.1';
      }
    },
    {
      type: 'input',
      name: 'description',
      message: "Description:",
      default: function () {
        return '';
      }
    },
    {
      type: 'input',
      name: 'main',
      message: "Entry Point:",
      default: function () {
        return 'src/index.js';
      }
    },
    {
      type: 'input',
      name: 'scripts.test',
      message: "Test Command:",
      default: function () {
        return '';
      }
    },
    {
      type: 'input',
      name: 'git.url',
      message: "Git Repository:",
      default: function () {
        return '';
      }
    },
    {
      type: 'input',
      name: 'keywords',
      message: "Keywords:",
      default: function () {
        return '';
      }
    },
    {
      type: 'input',
      name: 'author',
      message: "Author:",
      default: function () {
        return '';
      }
    },
    {
      type: 'input',
      name: 'licence',
      message: "Licence:",
      default: function () {
        return 'MIT';
      }
    },
  ];
  if ( opts.useDefaults ) {
    let answers = addDefaults();
    createPkgJson( {
      file: opts.path,
      obj: answers
    } )
  } else {
    inquirer.prompt( questions ).then( answers => {
      addDependencies( answers );
      addScripts( answers );
      createPkgJson( {
        file: opts.path,
        obj: answers
      } )
    } );
  }
}

function createPkgJson( opts ) {
  jsonfile.writeFile(
    path.join( opts.file, 'package.json' ),
    opts.obj, {
      spaces: 2
    },
    function ( err ) {
      if ( err != null ) {
        console.log( err );
      } else {
        console.log( `Successfully Created Boilerplate in ${opts.file}` )
      }
    } )
}

function addDefaults() {
  let pkgJson = {};
  pkgJson[ 'name' ] = parsedPath.base;
  pkgJson[ 'version' ] = "0.0.1";
  pkgJson[ 'description' ] = "This bot was built with fnc-boiler.";
  pkgJson[ 'main' ] = "./src/index.js";
  pkgJson = addScripts( pkgJson )
  pkgJson[ 'keywords' ] = [];
  pkgJson[ 'author' ] = "";
  pkgJson[ 'license' ] = "MIT";
  pkgJson = addDependencies( pkgJson );

  return pkgJson;
}

function addScripts( pkgJson ) {
  if ( pkgJson.scripts != undefined ) {
    if ( pkgJson.scripts.test === '' ) {
      pkgJson.scripts.test = 'echo \"Error: no test specified\" && exit 1';
    }
  } else {
    pkgJson.scripts = {
      'test': 'echo \"Error: no test specified\" && exit 1',
      'start': 'nodemon src/index.js --harmony',
      'publish': 'gitu'
    };
  }

  return pkgJson;
}

function addDependencies( pkgJson ) {
  var dependencies = {};
  dependencies[ 'common-tags' ] = "^1.5.1";
  dependencies[ 'discord.js' ] = "^11.2.1";
  dependencies[ 'discord.js-commando' ] = "^0.9.0";
  dependencies[ 'dotenv' ] = "^4.0.0";
  dependencies[ 'erlpack' ] = "hammerandchisel/erlpack";
  dependencies[ 'fnc-utils' ] = "^1.0.2";
  dependencies[ 'fs-extra' ] = "^4.0.3";
  dependencies[ 'git-upload' ] = "^1.1.4";
  dependencies[ 'libsodium-wrappers' ] = "^0.7.2";
  dependencies[ 'node-opus' ] = "^0.2.7";
  dependencies[ 'nodemon' ] = "^1.12.5";
  dependencies[ 'rekuire' ] = "^0.1.9";
  dependencies[ 'sqlite' ] = "^2.9.0";
  dependencies[ 'uws' ] = "^9.14.0";
  dependencies[ 'windows-build-tools' ] = "^1.3.2"


  pkgJson.dependencies = dependencies;
  return pkgJson
}

// Allows us to call this function from outside of the library file.
// Without this, the function would be private to this file.
exports.runNPMInit = runNPMInit;
