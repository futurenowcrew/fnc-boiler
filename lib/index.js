/**
 * @Author: Fabre Ed
 * @Date:   2017-12-18T11:02:11-05:00
 * @Email:  edwidgefabre@gmail.com
 * @Filename: index.js
 * @Last modified by:   Fabre Ed
 * @Last modified time: 2017-12-21T19:33:15-05:00
 */

var runInit = require('./run_boiler_init').runInit;
var runPing = require('./run_ping').runPing;


// Allows us to call this function from outside of the library file.
// Without this, the function would be private to this file.
exports.runPing = runPing;
exports.runInit = runInit;
