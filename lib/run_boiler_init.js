const inquirer = require('inquirer');
const path = require('path');
const fs = require('fs-extra');
var runNPMInit = require('./run_npm_init').runNPMInit;



function runInit(opts) {

  if (hasDefaultFlag(opts.flags)) {
    if (hasTestFlag(opts.flags)) {
      createFolderStructure({
        isTest: true,
        path: opts.path,
        useDefaults: true
      });
    } else {
      createFolderStructure({
        isTest: false,
        path: opts.path,
        useDefaults: true
      });
    }
  } else {
    var initPrompt = {
      type: 'confirm',
      name: 'isTest',
      message: "Would you like to test run the init script?",
      default: function() {
        return false;
      }
    }

    inquirer.prompt(initPrompt).then(answer => {
      createFolderStructure({
        isTest: answer.isTest,
        path: opts.path,
        useDefaults: false
      });
    });
  }
}

function hasDefaultFlag(flags) {
  if (flags !== undefined) {
    if (flags.includes('-d') || flags.includes('-default')) {
      return true;
    }
  }
  return false;
}

function hasTestFlag(flags) {
  if (flags !== undefined) {
    if (flags.includes('-t') || flags.includes('-test')) {
      return true;
    }
  }
  return false;
}

function createFolderStructure(opts) {
  var rootPath = opts.path;

  if (opts.isTest) {
    rootPath = path.join(opts.path, 'boilertest/');
  }

  const src = path.join(__dirname, 'root/');
  fs.copy(src, rootPath, err => {
    if (err) return console.error(err)
    runNPMInit({
      useDefaults: opts.useDefaults,
      path: rootPath
    });
  })
}

// Allows us to call this function from outside of the library file.
// Without this, the function would be private to this file.
exports.runInit = runInit;
