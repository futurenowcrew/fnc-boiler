require('dotenv').config();
const Commando = require('discord.js-commando');

const client = new Commando.Client({
  owner: process.env.BOT_OWNER,
  prefix: process.env.BOT_PREFIX
});

const path = require('path');

client.registry
  // Registers your custom command groups
  .registerGroups([
    ['math', 'Math Commands']
  ])

  // Registers all built-in groups, commands, and argument types
  .registerDefaults()

  // Registers all of your commands in the ./commands/ directory
  .registerCommandsIn(path.join(__dirname, 'commands'));

const sqlite = require('sqlite');

client.setProvider(
  sqlite.open(path.join(__dirname, 'config/settings.sqlite3')).then(db => new Commando
    .SQLiteProvider(db))
).catch(console.error);

// Set your BOT_TOKEN environment variable using .env file.
client.login(process.env.BOT_TOKEN);
