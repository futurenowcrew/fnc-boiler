#!/usr/bin/env node

var inquirer = require('inquirer');
var readJson = require('read-package-json')
var path = require('path');

const data = require('../config/data.json');
var lib = require('../lib/index.js');
var currPath = process.cwd();

// Delete the 0 and 1 argument (node and script.js)
var args = process.argv.splice(process.execArgv.length + 2);

var command = parseCommand(args);

if (command.name == 'init') {
  runInit();
} else if (command.name == 'ping') {
  runPing();
} else if (command.name == 'publish') {
  runPublish();
} else {
  runDefault();
}



function runDefault() {
  var defaultPrompt = {
    type: 'list',
    name: 'default',
    message: 'Please select a command.',
    choices: ['init', 'ping', 'publish']
  };

  inquirer.prompt(defaultPrompt).then(answers => {
    if (answers.default === 'init') {
      runInit();
    }
    if (answers.default === 'ping') {
      runPing();
    }
    if (answers.default === 'publish') {
      runPublish();
    }
  });
}

function runPublish() {
  hasPackageJson().then(
    (doneCallbacks) => {
      console.log(doneCallbacks);
    }, (failCallbacks) => {
      console.log(failCallbacks);
    });
}

function hasPackageJson() {
  return new Promise(function(resolve, reject) {
    var pkg = path.join(currPath, 'package.json');

    readJson(pkg, console.error, false, function(er, data) {
      if (er === undefined) {
        console.log("This Package doesn't have a package.json.");
        reject(false);
      } else {
        resolve(data);
      }
    });
  });
}

// Imported Functions
function runPing() {
  lib.runPing('World');
}

function runInit() {
  if (hasValidFlags()) {
    lib.runInit({
      path: currPath,
      flags: command.flags
    });
  } else {
    lib.runInit({
      path: currPath
    });
  }
}

function hasValidFlags() {
  let tempArr = command.flags;
  if (tempArr.length == 0) {
    return false;
  } else {
    for (var i = 0; i < tempArr.length; i++) {
      if (!isValidFlag(tempArr[i])) {
        console.error(`Error: '${tempArr[i]}' is not a valid flag.`);
        return false;
      }
    }
  }
  return true;
}

function isValidFlag(flag) {
  // These are the current valid flags
  for (var i = 0; i < data.availableFlags.length; i++) {
    if (data.availableFlags[i] == flag) {
      return true;
    }
  }
  return false;
}

function parseCommand(args) {
  let flagsArr = [];
  for (var i = 1; i < args.length; i++) {
    if (args[i].startsWith('-')) {
      flagsArr.push(args[i]);
    }
  }
  return {
    name: args[0],
    flags: flagsArr,
  }
}
