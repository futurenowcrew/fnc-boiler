# FutureNOW Bot Boiler Template

This module is used to create boilerplate for FutureNOW bots

## Getting Started

### Prerequisites

* [NodeJS](https://nodejs.org/en/) - JavaScript Runtime

### Installing


```sh
npm install -g fnc-boiler
cd path/to/your/bot/
fnc-boiler init -p
npm start
```

You need to modify the package.json file and set BOT_OWNER, BOT_TOKEN and BOT_PREFIX environment variables within a .env file in root directory.

## Running the tests

TBA

## Contributing

TBA

## Versioning

TBA

## Authors

* **Ed Fabre** - *Dev* - [Ed's Github](https://github.com/EdFabre) | [Ed's Bitbucket](https://bitbucket.org/enfabre/)
* **Ricky Sokel** - *Dev* - [Ricky's Github](https://github.com/vulpcod3z) | [Ricky's Bitbucket](https://bitbucket.org/vulpz/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

TBA
